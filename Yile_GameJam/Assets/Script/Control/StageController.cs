﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class StageController : MonoBehaviour
{
    [Header("關卡設定")]
    [SerializeField, Range(0, 2)] private int UseStageIndex;
    [Space(5f)]
    public CameraFlow cameraFlow;
    public ResultController resultController;
    public TextMeshProUGUI text_time;
    public StageInfo[] setting;
    private StageInfo useSettingInfo;
    public GameObject finishBanner;
    private float startTime;
    [SerializeField, Range(1, 2)]
    private int playerCount;
    [SerializeField]
    private ClamComtroller clamPrefab;
    private ClamComtroller[] joinClam;

    private List<PassStageRecord> recordList;
    private bool GameOver = false;
    private void Start()
    {
        RefreshStage();

    }

    private void ResetPlayer()
    {
        if (joinClam == null || joinClam.Length == 0)
        {
            joinClam = new ClamComtroller[playerCount];
            Transform[] clam = new Transform[playerCount];
            for (int i = 0; i < playerCount; i++)
            {
                var newClam = Instantiate<ClamComtroller>(clamPrefab);
                joinClam[i] = newClam;
                newClam.passStage = false;
                newClam.transform.position = useSettingInfo.start[i].position;
                newClam.ResetClam((PlayerType)i);
                clam[i] = newClam.entity;
            }
            cameraFlow.SetClams(clam);
        }
        else
        {
            for (int i = 0; i < playerCount; i++)
            {
                joinClam[i].passStage = false;
                joinClam[i].transform.position = useSettingInfo.start[i].position;
                joinClam[i].ResetClam((PlayerType)i);
            }
        }
    }

    private void Update()
    {
        if (!GameOver)
            UpdateTime();
        else
            WaitPressSpace();
    }

    private void WaitPressSpace()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            UseStageIndex += 1;
            RefreshStage();
        }
    }

    private void RefreshStage()
    {
        resultController.SetResultActive(false);
        recordList = new List<PassStageRecord>();
        GameOver = false;
        OpenState(UseStageIndex);
        ResetPlayer();
    }

    private void FixedUpdate()
    {
        CheckClamArriveGoal();
    }

    private void CheckClamArriveGoal()
    {
        int passCount = 0;
        for (int i = 0; i < joinClam.Length; i++)
        {
            var curClam = joinClam[i];
            if (curClam.entity.transform.position.x >= useSettingInfo.end.position.x && !curClam.passStage)
            {
                passCount += 1;
                curClam.passStage = true;
                PassStageRecord record = new PassStageRecord();
                record.player = curClam.plyerType;
                record.PassTime = text_time.text;
                SetPassStageRecord(record);
            }
        }

        if (passCount == joinClam.Length)
        {
            GameOver = true;
            resultController.SetResultActive(true);
            resultController.ShowResult(recordList);
        }
    }

    private void SetPassStageRecord(PassStageRecord record)
    {
        recordList.Add(record);
    }

    private void OpenState(int stageIndex)
    {
        for (int i = 0; i < setting.Length; i++)
        {
            if (stageIndex == i)
                setting[i].Stage.SetActive(true);
            else
                setting[i].Stage.SetActive(false);
        }
        useSettingInfo = setting[stageIndex];
    }

    private void UpdateTime()
    {
        float timeElapsed = Time.time - startTime;
        int minutes = (int)timeElapsed / 60;
        int seconds = (int)timeElapsed % 60;
        // int milliseconds = (int)(timeElapsed * 1000) % 1000;
        text_time.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    private void ResetTimeText()
    {
        text_time.text = "0:00";
        startTime = Time.time;
    }
}

[System.Serializable]
public class PassStageRecord
{
    public PlayerType player;
    public string PassTime;
}

[System.Serializable]
public class StageInfo
{
    public GameObject Stage;
    public Transform[] start;
    public Transform end;
}
