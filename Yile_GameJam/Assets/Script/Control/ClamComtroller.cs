﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

public class ClamComtroller : MonoBehaviour
{
    public bool passStage = false;

    public ClamSetting setting;
    public ClamType clamType;
    [SerializeField]
    private CollsionPointer pointer_top;
    [SerializeField]
    private CollsionPointer pointer_buttom;
    [SerializeField]
    public Transform entity;
    [SerializeField]
    private SpriteRenderer label;
    [SerializeField]
    private Vector2 entityOffset;
    public PlayerType plyerType;
    private Rigidbody2D rb2DOnGround;
    private ClamControllSetting curSetting;
    private float lerpValue = 0;
    [SerializeField]
    private float skillCD = 3f;
    [SerializeField]
    private ParticleSystem fx_skill;
    [SerializeField]
    private AudioClip sfx_ha;
    Tween fxTween;
    private bool canUseSkill;
    private void Start()
    {
        fx_skill.gameObject.SetActive(false);
        canUseSkill = true;
    }

    private void UseSkill()
    {
        if (!canUseSkill)
            return;

        canUseSkill = false;
        fx_skill.gameObject.SetActive(true);
        fx_skill.transform.position = (pointer_top.rb2D.position + pointer_buttom.rb2D.position) / 2;
        fx_skill.Play();
        DelayTween(skillCD, () => canUseSkill = true);
        AudioSource.PlayClipAtPoint(sfx_ha, entity.position);
    }

    private void DelayTween(float delay, Action callBack)
    {
        if (fxTween != null)
            fxTween.Kill();
        fxTween = DOVirtual.DelayedCall(delay, () => callBack.Invoke());
    }

    public void ResetClam(PlayerType type)
    {
        plyerType = type;
        clamType = ClamType.Close;
        curSetting = GetSetting();
        label.sprite = curSetting.playerLabel;
        gameObject.layer = LayerMask.NameToLayer(curSetting.layerTag);
        var child = GetComponentsInChildren<Rigidbody2D>();
        for (int i = 0; i < child.Length; i++)
        {
            child[i].gameObject.layer = LayerMask.NameToLayer(curSetting.layerTag);
        }
    }

    private void FixedUpdate()
    {
        if (!passStage)
            OnKeyDown(curSetting);
    }

    private void Update()
    {
        //LogPoint();
        CheckTouchGround();
        EntityFLow();
    }

    private void EntityFLow()
    {
        entity.transform.position = (pointer_top.rb2D.position + pointer_buttom.rb2D.position) / 2 + entityOffset;
    }

    private ClamControllSetting GetSetting()
    {
        for (int i = 0; i < setting.controllSettings.Length; i++)
        {
            if (setting.controllSettings[i].player == plyerType)
            {
                return setting.controllSettings[i];
            }
        }
        return null;
    }

    private void LogPoint()
    {
        if (pointer_top.enterPoints == null)
            return;

        for (int i = 0; i < pointer_top.enterPoints.point2Ds.Length; i++)
        {
            Debug.Log(string.Format("ArrayLength:{0} ,EnterPoint:{1} ", pointer_top.enterPoints.point2Ds.Length, pointer_top.enterPoints.point2Ds[i].point));
        }
    }

    private void CheckTouchGround()
    {
        if (pointer_top.isTouch && pointer_buttom.isTouch)
        {
            //if (rb2DOnGround != null)
            //    Debug.Log("rb2DOnGround: " + rb2DOnGround.name);
            return;
        }
        else if (pointer_top.isTouch && !pointer_buttom.isTouch)
        {
            rb2DOnGround = pointer_top.rb2D;
            return;
        }
        else if (!pointer_top.isTouch && pointer_buttom.isTouch)
        {
            rb2DOnGround = pointer_buttom.rb2D;
            return;
        }
        else if (!pointer_top.isTouch && !pointer_buttom.isTouch)
        {
            // rb2DOnGround = null;
            //DecreaseTorque();
            return;
        }
    }

    public void OnKeyDown(ClamControllSetting setting)
    {
        if (Input.GetKey(setting.left))
        {
            MoveEvent(-1);
        }
        if (Input.GetKey(setting.right))
        {
            MoveEvent(1);
        }

        if (Input.GetKey(setting.open))
        {
            OpenEvent();
        }
        else
        {
            CloseEvent();
        }

        if (Input.GetKeyDown(setting.skill))
        {
            UseSkill();
        }
    }

    public void MoveEvent(float dir)
    {
        if (rb2DOnGround == null)
            return;

        rb2DOnGround.AddTorque(-dir * setting.moveForce);
    }


    private void RotateShellEvent()
    {
        var t = setting.openCurve.Evaluate(lerpValue / setting.closeDuration) * setting.maxOpenAngle;
        var rotationTop = pointer_top.rb2D.rotation;
        var rotationButtom = pointer_buttom.rb2D.rotation;
        if (t != setting.maxOpenAngle)
        {
            pointer_top.rb2D.MoveRotation(rotationTop + t);
            pointer_buttom.rb2D.MoveRotation(rotationButtom - t);
        }
    }

    public void OpenEvent()
    {
        clamType = ClamType.Open;
        if (lerpValue < setting.openDuration)
            lerpValue += Time.deltaTime;
        RotateShellEvent();
        Debug.Log(string.Format("Duration{0}", lerpValue));
    }


    public void CloseEvent()
    {
        if (clamType == ClamType.Open)
        {
            lerpValue -= Time.deltaTime;
            if (lerpValue <= 0)
                clamType = ClamType.Close;
            RotateShellEvent();
        }
    }
}


public enum ClamType
{
    Open = 0,
    OpenIdle,
    Close
}
public enum PlayerType
{
    Player1 = 0,
    Player2
}
