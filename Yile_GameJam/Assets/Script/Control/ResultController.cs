﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultController : MonoBehaviour
{
    public GameObject canvas;
    public GameObject[] recordItem;
    public Text[] PlayerText;
    public Text[] TimeText;
    public void SetResultActive(bool active)
    {
        canvas.SetActive(active);
    }

    public void ShowResult(List<PassStageRecord> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            recordItem[i].SetActive(true);
            PlayerText[i].text = list[i].player.ToString();
            TimeText[i].text = list[i].PassTime;
        }
    }
}
