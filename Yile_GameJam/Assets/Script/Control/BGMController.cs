﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMController : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private AudioClip bgm_choseClam;
    [SerializeField]
    private AudioClip[] bgm_stage;
    void Start()
    {
        if (bgm_choseClam != null)
            AudioSource.PlayClipAtPoint(bgm_choseClam, transform.position);
    }
}
