﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFlow : MonoBehaviour
{
    [SerializeField]
    private Transform[] clams;
    [SerializeField]
    private Vector3 offset = new Vector3(0, 0, -10f);
    [SerializeField]
    private Vector3[] limitSidePos = new Vector3[] { new Vector3(-6.7f, -3.7f, 0), new Vector3(6.6f, 3.3f, 0) };
    private float timeRecord = 0;
    [SerializeField]
    private float smoothSpeed = 0.125f;
    private Vector3 curCameraPos;

    public bool GameStart;

    public void SetClams(Transform[] clams) { this.clams = clams; }

    void Start()
    {
        CameraDefaultPos();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void CameraDefaultPos()
    {
        Camera.main.transform.position = limitSidePos[0];
    }

    private void FixedUpdate()
    {
        if (GameStart)
            Camera.main.transform.position = LerpAimPos(GetAimPos());
    }

    private Vector3 LerpAimPos(Vector3 targetPos)
    {
        curCameraPos = GetUsingPos(targetPos);

        return Vector3.Lerp(Camera.main.transform.position, curCameraPos, smoothSpeed);
    }

    private Vector3 GetUsingPos(Vector3 targetPos)
    {
        Vector3 result = new Vector3(0, 0, offset.z);
        if (targetPos.x >= limitSidePos[0].x && targetPos.x <= limitSidePos[1].x)
            result.x = targetPos.x;
        else
            result.x = Camera.main.transform.position.x;

        if (targetPos.y >= limitSidePos[0].y && targetPos.y <= limitSidePos[1].y)
            result.y = targetPos.y;
        else
            result.y = Camera.main.transform.position.y;

        return result;
    }

    private Vector3 GetAimPos()
    {
        Vector3 pos;
        if (clams.Length == 1)
        {
            pos = clams[0].transform.position;
        }
        else if (clams.Length > 1)
        {
            pos = clams[0].transform.position - clams[1].transform.position / 2;
        }
        else
        {
            pos = Vector3.zero;
        }
        Debug.Log("CameraPos: " + pos + offset);
        return pos + offset;

    }
}
