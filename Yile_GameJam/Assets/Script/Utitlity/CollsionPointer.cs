﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollsionPointer : MonoBehaviour
{
    public Collider2D co2D;
    public Rigidbody2D rb2D;

    public TimeStampContacts enterPoints;
    public TimeStampContacts exitPoints;
    public TimeStampContacts stayPoints;

    public bool isTouch;
    private void Awake()
    {
        co2D = transform.GetComponent<Collider2D>();
        rb2D = transform.GetComponent<Rigidbody2D>();
    }

    public void MoveRotation(float angle)
    {
        rb2D.MoveRotation(angle);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        enterPoints = new TimeStampContacts();
        enterPoints.point2Ds = collision.contacts;
        enterPoints.time = System.DateTime.Now;
        isTouch = true;
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        exitPoints = new TimeStampContacts();
        exitPoints.point2Ds = collision.contacts;
        exitPoints.time = System.DateTime.Now;  
    }

    public void OnCollisionStay2D(Collision2D collision)
    {
        stayPoints = new TimeStampContacts();
        stayPoints.point2Ds = collision.contacts;
        stayPoints.time = System.DateTime.Now;

        isTouch = false;
    }


}

[System.Serializable]
public class TimeStampContacts
{
    public System.DateTime time;
    public ContactPoint2D[] point2Ds;
}
