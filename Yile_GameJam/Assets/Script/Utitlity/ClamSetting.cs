﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CalmSetting")]
public class ClamSetting : ScriptableObject
{
    public float moveForce = 1f;
    public float jumpForce = 2f;

    public float maxOpenAngle;
    public AnimationCurve openCurve;
    public AnimationCurve closeCurve;
    public float openDuration;
    public float closeDuration;
    public ClamControllSetting[] controllSettings;  
}

[System.Serializable]
public class ClamControllSetting
{
    public PlayerType player;
    public Sprite playerLabel;
    public string layerTag;
    public KeyCode left;
    public KeyCode right;
    public KeyCode open;
    public KeyCode skill;
}
